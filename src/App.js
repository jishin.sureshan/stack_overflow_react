import './App.css';
import React, {useState} from 'react';
import Login from './components/login';
import Stack from './components/stack';
import { useAlert } from 'react-alert';

function App() {
  const [token, setToken] = useState('');
  const [user, setUser] = useState('');
  const alert = useAlert()
  const userLogin = (tok) =>{
    setToken(tok)
  }
  const userName = (data) =>{
    setUser(data)
  }
  const userAlerts = (error) =>{
    alert.show(error);
  }
  return (
    <div className="App">
      <header className="App-header">
        {!token?(<Login userLogin={userLogin} userName={userName}
        userAlerts={userAlerts}/>):(<Stack userName={user}/>)}
      </header>
    </div>
  );
}

export default App;
