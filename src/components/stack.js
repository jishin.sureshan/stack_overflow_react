import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import './pagination.css';

class Stack extends Component {
    state = {
        query: { search: '', closed: '', page: '', upto: '' },
        response: [],
        offset: 0,
        tableData: [],
        orgtableData: [],
        perPage: 5,
        currentPage: 0,
        pageCount: 0
    }

    handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;

        this.setState({
            currentPage: selectedPage,
            offset: offset
        }, () => {
            this.loadMoreData()
        });

    };

    loadMoreData() {
		const data = this.state.response;
		console.log("here:", data)
		const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
		this.setState({
			pageCount: Math.ceil(data.length / this.state.perPage),
			tableData:slice
		})
	
    }


    handleChange = event => {
        console.log(event.target.value)
        const cred = this.state.query;
        cred[event.target.name] = event.target.value
        this.setState({ query: cred })
    }

    get_stack = (e) => {
        console.log(this.state.query)
        fetch(`http://127.0.0.1:8000/stack/?search=${this.state.query.search}&closed=${this.state.query.closed}&page=${this.state.query.page}&pagecount=${this.state.query.upto}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Token ${localStorage.getItem('token')}`

            },
        }).then(
            (data) =>
                data.json()

        ).then(
            (data) => { this.setState({ response: data }) }
        ).then(res => {
            var tdata = this.state.response;
             var slice = tdata.slice(this.state.offset, this.state.offset + this.state.perPage)
            this.setState({
                pageCount: Math.ceil(tdata.length / this.state.perPage),
                orgtableData : tdata,
                tableData:slice
            })
        }).catch(
            error => {
                console.log(error)
            }
        )
        console.log(e)
    }
    render() {
        var ex = []
        if (this.state.tableData){
            ex = this.state.tableData;
        }
        else{
            ex = []
        }
        const row = ex.map((e) =>
            <tr key={e.owner.account_id}>
                <td style={{
                    border: "1px solid #dddddd",
                    textAlign: "center",
                    padding: "8px"
                }}>
                    <h7><em>{e.title}</em></h7>
                    <p style={{fontSize: "15px"}}>Link: <a href={e.link} rel="noreferrer"target="_blank">{e.link}</a></p>
                    <p style={{fontSize: "15px"}}>Answered: {e.answer_count}</p></td>
            </tr>);
        return (
            <div className="App">
                <div>
                <h1>Search Stack overflow</h1>
                <input style={{width:"100%", height:"20px"}} type="text" name="search" onChange={this.handleChange} value={this.state.query.search} /><br />
                <br/><label for="html">Closed answer</label> 
                <input type="radio" name="closed" value="False" onChange={this.handleChange}/>
                <input type="radio" name="closed" value="True" onChange={this.handleChange}/>
                <br/>
                <input style={{width:"10%", height:"20px"}} type="text" name="page" onChange={this.handleChange} value={this.state.query.page} />page  <input style={{width:"10%", height:"20px"}} type="text" name="upto" onChange={this.handleChange} value={this.state.query.upTo} />upto<br />
                <br/>
                <button onClick={this.get_stack}>search</button>
                </div>
                <table style={{
                    fontFamily: "arial, sans-serif",
                    borderCollapse: "collapse",
                    width: "100%",
                    alignContent: "center",
                    marginTop: "20px"
                }}>
                    <tbody>
                        <tr>
                            <th style={{
                                border: "1px solid #dddddd",
                                textAlign: "center",
                                padding: "8px"
                            }}>Result</th>
                        </tr>
                        {row.length ?(row):('Please search on stackoverflow')}
                    </tbody>
                </table>
                <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
            </div>
        );

    }


}

export default Stack;
