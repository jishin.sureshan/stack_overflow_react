import React, {Component } from 'react';


class Login extends Component {
    state = {
        credentials : {username: '', password: ''},
        response : []
    }

    handleChange = event =>{
        const cred = this.state.credentials;
        cred[event.target.name] = event.target.value
        this.setState({credentials: cred})
    }
    
    login = (e) =>{
        console.log(this.state.credentials)
        fetch('http://127.0.0.1:8000/auth/', {
            method: 'POST',
            headers: {'Content-Type': 'application/json',
            'Accept': 'application/json',},
            body: JSON.stringify(this.state.credentials)
        }).then(function(response){ return response.json(); })
        .then(function(data) {
            const items = data;
            console.log(items.token)
            localStorage.setItem('token', items.token)
        })
        .then(
            data =>{
                this.props.userLogin(localStorage.getItem('token'));
                this.props.userName(this.state.credentials.username)
                if (data.non_field_errors){
                    this.setState({response: data.non_field_errors[0]})
                    console.log(this.state.response)
                }
                else {
                    this.setState({response: []})
                }
            }
        ).catch(
            error=>{
                console.log(error)
            }

        )
       
    }
    register = (e) =>{
        console.log(this.state.credentials)
        fetch('http://127.0.0.1:8000/api/user/', {
            method: 'POST',
            headers: {'Content-Type': 'application/json',
            'Accept': 'application/json',},
            body: JSON.stringify(this.state.credentials)
        }).then(
            data=> {
                console.log(data)
            }
        ).catch(
            error=>{
                console.log(error)
            }
        )
        console.log(e)
    }
    render(){
        return (
            <div className="App">
                <h1>Login User</h1>
                  <label>Username: </label>
                  <input type="text" name="username" onChange={this.handleChange} value={this.state.credentials.username}/><br/>
                  <label>Password: </label>
                  <input type="text" name="password" onChange={this.handleChange} value={this.state.credentials.password}/><br/>
                  <button onClick={this.login}>Login</button> 
                  <button onClick={this.register}>Register</button> 
                  <p style={{color: 'red'}}>{this.state.response}</p>
            </div>
          );
    
        }


}

export default Login;
